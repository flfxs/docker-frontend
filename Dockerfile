FROM mhart/alpine-node:8
MAINTAINER Jethro Van Thuyne <post@jethro.be>

RUN npm i -g browserify@14.4.0 \
             uglify-js@3.1.2 \
             less@2.7.2 \
             less-plugin-clean-css@1.5.1 \
             pug@2.0.0-rc.4 \
             pug-cli@1.0.0-alpha6 && \
    npm cache clean --force

VOLUME ["/data"]
