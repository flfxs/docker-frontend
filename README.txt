Frontend Build Tools
====================

A Docker container with some frontend build tools:


Less
----

- Add the snippet to `~/bin/less`.
- Then run `less path/to/_build.less > path/to/main.min.css`.

  #!/bin/bash
  docker run -it -v $(pwd):/data 0x0398/docker-fbt \
      lessc --verbose --clean-css="--s1 --advanced" \
      /data/$1


Pug
---

- Add the snippet to `~/bin/pug`.
- Then run `pug path/to/file.pug`.

  #!/bin/bash
  docker run -it -v $(pwd):/data 0x0398/docker-fbt \
      pug \
      /data/$1


Browserify
----------

- Add the snippet to `~/bin/browserify`.
- Then run `browserify path/to/main.js > path/to/build.js`.

  #!/bin/bash
  docker run -it -v $(pwd):/data 0x0398/docker-fbt \
      browserify \
      /data/$1


UglifyJS
--------

- Add the snippet to `~/bin/uglifyjs`.
- Then run `uglifyjs path/to/build.js > path/to/build.min.js`.

  #!/bin/bash
  docker run -it -v $(pwd):/data 0x0398/docker-fbt \
      uglifyjs
      /data/$1 -m
